﻿using System.Collections;
using System.Collections.Generic;
using HollowFall.Manager;
using UnityEngine;

namespace HollowFall.Entity
{
    [SerializeField]
    public enum BallType
    {
        Red,
        Green
    }
    public class Ball : MonoBehaviour
    {
        [SerializeField] private BallType ballType;
        private Transform thisTransform;
        public void FallDown(Transform fallPoint)
        {
            thisTransform = transform;
            GameManager.Instance.AddPoint(ballType == BallType.Green ? 2 : -1);
            StartCoroutine(DoFalling(fallPoint));
        }

        private IEnumerator DoFalling(Transform fallPsoition)
        {
            var lerpFactor = 0f;
            var life = 1f;
            var endTime = Time.time + life;
            while (lerpFactor<1f)
            {
                lerpFactor = 1f - Mathf.Clamp01((endTime - Time.time) / life);
                thisTransform.position = Vector3.Lerp(thisTransform.position, fallPsoition.position, lerpFactor);
                yield return new WaitForEndOfFrame();
            }
            DestoryBall();
        }

        private void DestoryBall()
        {
            Destroy(gameObject);
        }
        
    }
}
