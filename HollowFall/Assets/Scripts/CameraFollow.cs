﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HollowFall.Entity
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform targetTransform;
        [SerializeField] private float offset=6f;
        [SerializeField] private float dampingTime = 1f;
        private Transform thisTransform;
        private Vector3 velocity = Vector3.forward;

        private void Awake()
        {
            thisTransform = transform;
        }

        private void LateUpdate()
        {
            var position= new Vector3(thisTransform.position.x, thisTransform.position.y,
                              targetTransform.position.z + offset);
            thisTransform.position = Vector3.SmoothDamp(thisTransform.position, position, ref velocity,
                dampingTime);
        }
    }
}