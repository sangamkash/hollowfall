﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HollowFall.Constant
{
    public class GameConstants 
    {
       //Tags
       public const string Tag_Ball = "Ball";

       public const string Scene_Start = "Start";

    }
}

namespace Util
{
    public static class Utility
    {
        public static int GetLayerFromMask(this LayerMask aMask)
        {
            uint val = (uint)aMask.value;
            if (val  == 0)
                return -1;
            for (int i = 0; i < 32; i++)
            {
                if( (val & (1<<i)) != 0)
                    return i;
            }
            return -1;
        }
    }
}