﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HollowFall.Manager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        [SerializeField] private GameObject holeController;
        [SerializeField] private Transform holeTransform;
        [SerializeField] private UIManager UiManager;
        [SerializeField] private float GameOverLimit;
        private int score;
        private bool gameOver;

        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public void AddPoint(int point)
        {
            score += point;
            UiManager.UpdateScore(score);
        }

        private void Update()
        {
            if (gameOver)
                return;
            gameOver = holeTransform.position.z > GameOverLimit;
            if (gameOver)
                GameOver();
        }

        private void GameOver()
        {
            holeController.SetActive(false);
            UiManager.GameOver();
        }
    }
}
