﻿using System;
using System.Collections;
using System.Collections.Generic;
using HollowFall.Constant;
using HollowFall.Entity;
using UnityEngine;
using Util;
namespace HollowFall.Entity
{
    public class Hole : MonoBehaviour
    {
        [SerializeField] private Transform fallPoint;
        [SerializeField] private LayerMask BallLayer;
        [SerializeField] private LayerMask HoleLayer;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == BallLayer.GetLayerFromMask())
            {
                var gameObj=other.gameObject;
                gameObj.layer = HoleLayer.GetLayerFromMask();
                gameObj.GetComponent<Ball>().FallDown(fallPoint);
            }
        }
    }
}
