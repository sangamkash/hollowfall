﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace HollowFall.Manager
{
    public class HoleConroller : MonoBehaviour
    {
        [SerializeField] private float sensitivity=1f; 
        [SerializeField] private Transform targetHole;
        [SerializeField] private Transform ground;
        [SerializeField] private float speed=2f;
        [SerializeField] private float radiusOfHole = 2f;
        private Vector3 lastmousePosition;
        private Vector2 lastTouchPosition;
        private int touchID=-1;
        
        private void Update()
        {
            targetHole.position += Vector3.forward * speed * Time.deltaTime;

#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
                var delta = Input.mousePosition - lastmousePosition;
                ApplyInput(delta);
            }
            lastmousePosition = Input.mousePosition;
#else
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    var touch= Input.GetTouch(i);
                    if (touch.fingerId == touchID)
                    {
                        var delta = touch.position - lastTouchPosition;
                        ApplyInput(delta);
                        lastTouchPosition = touch.position;
                    }

                    if (touch.phase == TouchPhase.Began && touchID == -1)
                    {
                        touchID = touch.fingerId;
                        lastTouchPosition = touch.position;
                    }

                    if ((touch.fingerId == touchID) &&
                        (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled))
                    {
                        touchID = -1;
                    }


                }
            }
#endif

            
        }

        private void ApplyInput(Vector3 delta)
        {
            var modifiedDelta = new Vector3(delta.x, 0f, delta.y);
            targetHole.position += modifiedDelta * sensitivity;
            var xLimit = ground.position.x + ground.localScale.x / 2 ;
            var zLimit = ground.position.z + ground.localScale.z / 2;
            targetHole.position= new Vector3(Mathf.Clamp(targetHole.position.x,-xLimit + radiusOfHole,xLimit - radiusOfHole),targetHole.position.y,
                Mathf.Clamp(targetHole.position.z,-zLimit,zLimit));
        }
    }
}
