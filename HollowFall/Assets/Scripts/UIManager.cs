﻿using System;
using System.Collections;
using System.Collections.Generic;
using HollowFall.Constant;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HollowFall.Manager
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Text score;
        [SerializeField] private GameObject playBtnGameObj;
        [SerializeField] private GameObject retryBtnGameObj;
        [SerializeField] private GameObject holeControllerGameObj;

        public void Playbutton()
        {
            holeControllerGameObj.SetActive(true);
            playBtnGameObj.SetActive(false);
        }

        public void GameOver()
        {
            retryBtnGameObj.SetActive(true);
        }

        public void RetyButton()
        {
            SceneManager.LoadScene(GameConstants.Scene_Start);
        }

        public void UpdateScore(int value)
        {
            score.text = value.ToString();
        }
    }
}
